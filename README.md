# Device Tree for Meizu M6 Note (codenamed _m1721_)

Kernel source: "https://github.com/meizucustoms/android_kernel_meizu_m1721.git"
==================================
## Device specifications

| Feature                 | Specification                     |
| :---------------------- | :-------------------------------- |
| CPU                     | Octa-core 2.0 GHz Cortex-A53      |
| Chipset                 | Qualcomm MSM8953 Snapdragon 625   |
| GPU                     | Adreno 506                        |
| Memory                  | 3/4 GB                            |
| Shipped Android Version | 7.1.2                             |
| Storage                 | 16/32/64 GB                       |
| MicroSD                 | Up to 128 GB                      |
| Battery                 | 4000 mAh (non-removable)          |
| Dimensions              | 154.6 x 75.2 x 8.35 mm            |
| Display                 | 1920x1080 pixels, 5.5 (~401 PPI)  |
| Rear Camera             | 12 MP + 5 MP, Quad LED flash      |
| Front Camera            | 16 MP                             |
| Release Date            | August 2017                       |

## Device Picture

![Meizu M6 Note](https://static.onlinetrade.ru/img/items/b/smartfon_meizu_m6_note_16gb_black_743603_1.jpg "Meizu M6 Note")

### Copyright
 ```
  /*
  *  Copyright (C) 2013-2020 The TWRP
  *
  *  Copyright (C) 2018-2021 The OrangeFox Recovery Project
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  *
  */
  ```
